export const setActivePage = (page) => {
  return {
    type: 'SET_ACTIVE_PAGE',
    page,
  }
}

export const updateManageCashFlow = (params) => {
  return {
    type: 'UPDATE_MANAGE_CASH_FLOW',
    params
  }
}
