import { createStore as reduxCreateStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import layout from './layout'
import cashFlow from './cashFlow'

const rootReducer = combineReducers({
  layout,
  cashFlow,
})

const initialState = undefined

const createStore = () => reduxCreateStore(rootReducer, initialState, compose(
  applyMiddleware(thunkMiddleware),
  typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
))
export default createStore
