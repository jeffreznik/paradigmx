import {
  updateManageCashFlow
} from './actions'

const initialState = {
  score: 40,
  pac: true,
  creditCardDebt: false,
  mortgagePayments: true,
  savings: false,
  plannedAnnualSavings: 8400,
  actualAnnualSavings: 6200,
}

function cashFlow(state = initialState, action) {
  switch (action.type) {
    case updateManageCashFlow().type:
      return Object.assign({}, state, {
        score: action.params.score,
        savings: action.params.savings,
        actualAnnualSavings: state.actualAnnualSavings + parseInt(action.params.amount, 10),
      })
    default:
      return state
  }
}

export default cashFlow
