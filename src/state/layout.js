import {
  setActivePage
} from './actions'

const initialState = {
  activePage: 'home',
}

function layout(state = initialState, action) {
  switch (action.type) {
    case setActivePage().type:
      return Object.assign({}, state, {
        activePage: action.page,
      })
    default:
      return state
  }
}

export default layout
