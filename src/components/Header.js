import React from 'react'
import styled from 'styled-components'

const HeaderImage = styled.div`
  width: 100%;
  height: 33%;
  background-image: url('house-header.jpg');
  background-size: cover;
`
const Logo = styled.img`
  width: 80px;
  height: 40px;
  margin-top: 16px;
  margin-left: 16px;
`
const Headline = styled.p`
  color: #fff;
  font-size: 18px;
  font-weight: lighter;
  margin-left: 24px;
  margin-top: 32px;
`
function Header({ image }) {
  return (
    <HeaderImage>
      <Logo src="igm-logo.png" />
      <Headline>Closer to<br />my dream home</Headline>
    </HeaderImage>
  )
}

export default Header
