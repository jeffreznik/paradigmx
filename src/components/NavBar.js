import React from 'react'
import { withStyles } from '@material-ui/core'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import HomeIcon from '@material-ui/icons/Home'
import FavoriteIcon from '@material-ui/icons/Favorite'
import LocationOnIcon from '@material-ui/icons/LocationOn'

const styles = {
  root: {
    position: 'fixed',
    width: '100%',
    bottom: 0,
    zIndex: 5,
  }
}

class NavBar extends React.Component {
  constructor(props) {
    super(props)
    this.navButtons = [
      {
        label: 'Home',
        icon: <HomeIcon />,
        page: 'home',
      },
      {
        label: 'Favorites',
        icon: <FavoriteIcon />,
        page: 'favorites',
      },
      {
        label: 'Nearby',
        icon: <LocationOnIcon />,
        page: 'nearby',
      },
    ]
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(event, index) {
    this.props.onNavChange(this.navButtons[index].page)
  }
  render() {
    const { classes } = this.props
    return (
      <BottomNavigation
        showLabels
        onChange={this.handleChange}
        className={classes.root}
      >
      {this.navButtons.map(button => <BottomNavigationAction label={button.label} icon={button.icon} key={button.page} />)}
      </BottomNavigation>
    )
  }
}

export default withStyles(styles)(NavBar)
