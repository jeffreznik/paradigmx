import React from 'react'
import CircularProgressbar from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'

const red = '#ed145b'
const orange = '#f7941d'
const green = '#20d38f'

function RadialProgressBar({ percent, initialAnimation }) {
  let color;
  if (percent <= 40) {
    color = red
  } else if (percent <= 75) {
    color = orange
  } else {
    color = green
  }
  return (
    <CircularProgressbar
      percentage={percent}
      text={`${percent}%`}
      initialAnimation={initialAnimation}
      background
      strokeWidth={5}
      styles={{
        background: { fill: '#fff' },
        // Customize the root svg element
        root: {},
        // Customize the path, i.e. the part that's "complete"
        path: {
          // Tweak path color:
          stroke: color, // orange #f7941d, red #ed145b
          // Tweak path to use flat or rounded ends:
          strokeLinecap: 'butt',
          // Tweak transition animation:
          transition: 'stroke-dashoffset 0.5s ease 0s',
        },
        // Customize the circle behind the path
        trail: {
          // Tweak the trail color:
          stroke: '#fff',
        },
        // Customize the text
        text: {
          // Tweak text color:
          fill: '#172c4f',
          // Tweak text size:
          fontSize: '30px',
        },
      }}
    />
  )
}

export default RadialProgressBar
