import React from 'react'
import styled from 'styled-components'
import CheckIcon from '@material-ui/icons/Check'
import CloseIcon from '@material-ui/icons/Close'

const Container = styled.div`
  box-shadow: 0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19);
  border: solid 1px #ccc;
  width: 90%;
  margin: 16px auto;
`
const TagContainer = styled.div`
  display: flex;
`
const Label = styled.div`
  background-color: #fff;
  padding: 11px 8px;
  flex-grow: 1;
  color: ${props => props.green ? '#22d390' : '#ed195f'};
`
const Indicator = styled.div`
  background-color: #fff;
  padding: 8px;
  width: 40px;
  border-left: solid 1px #ccc;
  color: ${props => props.green ? '#22d390' : '#ed195f'};
`
const ContentContainer = styled.div`
  border-top: solid 1px #ccc;
  background-color: #fff;
  padding: 8px;
`
class FactorTag extends React.Component {
  render() {
    const { children, label, success, open, onClick } = this.props
    const indicatorIcon = success ? <CheckIcon /> : <CloseIcon />
    return (
      <Container>
        <TagContainer onClick={onClick}>
          <Label green={success}>{label}</Label>
          <Indicator green={success}>{indicatorIcon}</Indicator>
        </TagContainer>
        { open &&
          <ContentContainer>{children}</ContentContainer>
        }
      </Container>
    )
  }
}

export default FactorTag
