import React from 'react'
import styled from 'styled-components'
import RadialProgressBar from '../components/RadialProgressBar'
import FactorTag from '../components/FactorTag'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { connect } from 'react-redux'
import { updateManageCashFlow } from '../state/actions'

const Page = styled.div`
  background-color: #504a8a;
`
const Header = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  height: 33%;
  background-image: url('purple-people.png');
  background-size: cover;
`
const BasePlate = styled.div`
  background-image: url(gamma-baseplate.png);
  background-size: cover;
  position: relative;
  width: 100%;
  margin-top: 45%;
  height: 100%;
`
const ProgressBarPositioner = styled.div`
  width: 25%;
  position: absolute;
  left: 50%;
  margin-left: -50px;
  margin-top: -36px;
`
const ScreenTitle = styled.p`
  text-align: center;
  color: #fff;
  font-size: 12px;
  font-weight: bold;
`
const ContentPositioner = styled.div`
  padding-top: 50px;
  min-height: 425px;
`
const Label = styled.p`
  font-size: 10px;
`
const BottomButtonContainer = styled.div`
  display: flex;
  width: 100%;
  margin-top: 12px;
`
const ButtomButton = styled.a`
  text-align: center;
  display: inline-block;
  padding: 8px 12px;
  width: 50%;
  font-size: 14px;
  background-color: ${props => props.color || '#000'};
  color: #fff;
  cursor: pointer;
`
const mapStateToProps = state => {
  return {
    cashFlow: state.cashFlow
  }
}
const mapDispatchToProps = dispatch => {
  return {
    updateManageCashFlow: params => dispatch(updateManageCashFlow(params))
  }
}
class ManagingCashFlowScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      savingsType: null,
      savingsMoreInfo: '',
      savingsAmount: '',
      savingsFrequency: null,
      openSavings: false,
      savingsPage: 1,
    }
    this.handleSavingsTypeChange = this.handleSavingsTypeChange.bind(this)
    this.handleSavingsMoreInfoChange = this.handleSavingsMoreInfoChange.bind(this)
    this.handleSavingsAmountChange = this.handleSavingsAmountChange.bind(this)
    this.handleSavingsFrequencyChange = this.handleSavingsFrequencyChange.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)

    this.renderSavingsContentPage1 = this.renderSavingsContentPage1.bind(this)
    this.renderSavingsContentPage2 = this.renderSavingsContentPage2.bind(this)
  }

  handleSavingsTypeChange(type) {
    this.setState({ savingsType: type })
  }
  handleSavingsMoreInfoChange(event) {
    this.setState({ savingsMoreInfo: event.target.value })
  }
  handleSavingsAmountChange(event) {
    this.setState({ savingsAmount: event.target.value })
  }
  handleSavingsFrequencyChange(frequency) {
    this.setState({ savingsFrequency: frequency })
  }
  handleFormSubmit() {
    this.setState({ openSavings: false, savingsPage: 1 })
    this.props.updateManageCashFlow({ savings: true, score: 81, amount: this.state.savingsAmount })
  }
  openTag(tagName) {
    if (tagName === 'savings') {
      this.setState({ openSavings: !this.state.openSavings })
    }
  }

  renderSavingsContentPage1(planned, actual) {
    return(
      <>
        <table style={{ width: '80%', fontSize: '12px', margin: '0 auto' }}>
          <tr>
            <td>Your Planned Annual Savings</td>
            <td style={{ textAlign: 'right' }}>${planned}</td>
          </tr>
          <tr>
            <td>Your Actual Annual Savings</td>
            <td style={{ textAlign: 'right' }}>${actual}</td>
          </tr>
        </table>
        <BottomButtonContainer>
          <ButtomButton color="#f69d32" onClick={() => this.setState({ savingsPage: 2 })}>Add Information</ButtomButton>
          <ButtomButton color="#34d699">Call Bob</ButtomButton>
        </BottomButtonContainer>
      </>
    )
  }

  renderSavingsContentPage2() {
    const { savingsType, savingsMoreInfo, savingsAmount, savingsFrequency } = this.state
    return (
      <>
        <Label>Savings</Label>
        <Button
          variant="outlined"
          onClick={() => this.handleSavingsTypeChange('firm')}
          color={savingsType === 'firm' ? 'primary' : null}
        >
          Investment Firm
        </Button>
        <Button
          variant="outlined"
          onClick={() => this.handleSavingsTypeChange('bank')}
          color={savingsType === 'bank' ? 'primary' : null}
        >
          Bank
        </Button>
        <Button
          variant="outlined"
          onClick={() => this.handleSavingsTypeChange('other')}
          color={savingsType === 'other' ? 'primary' : null}
        >
          Other
        </Button>

        <TextField
          label="More Info"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          value={savingsMoreInfo}
          onChange={this.handleSavingsMoreInfoChange}
          margin="dense"
        />
        <TextField
          label="Amount Saved"
          margin="normal"
          variant="outlined"
          style={{ width: '100%' }}
          value={savingsAmount}
          onChange={this.handleSavingsAmountChange}
          type="number"
          margin="dense"
        />

        <Button
          variant="outlined"
          onClick={() => this.handleSavingsFrequencyChange('weekly')}
          color={savingsFrequency === 'weekly' ? 'primary' : null}
        >
          Weekly
        </Button>
        <Button
          variant="outlined"
          onClick={() => this.handleSavingsFrequencyChange('monthly')}
          color={savingsFrequency === 'monthly' ? 'primary' : null}
        >
          Monthly
        </Button>
        <Button
          variant="outlined"
          onClick={() => this.handleSavingsFrequencyChange('yearly')}
          color={savingsFrequency === 'yearly' ? 'primary' : null}
        >
          Yearly
        </Button>

        <BottomButtonContainer>
          <ButtomButton color="#f69d32" onClick={this.handleFormSubmit}>Add Information</ButtomButton>
          <ButtomButton color="#34d699">Call Bob</ButtomButton>
        </BottomButtonContainer>
      </>
    )
  }

  render() {
    const { openSavings, savingsPage } = this.state
    const { cashFlow } = this.props
    return (
      <Page>
        <Header />
        <BasePlate>
          <ProgressBarPositioner>
            <RadialProgressBar percent={cashFlow.score} initialAnimation />
          </ProgressBarPositioner>
          <ContentPositioner>
            <ScreenTitle>Managing Cash Flow</ScreenTitle>
            <FactorTag label="Pre-authorized contribution" success={cashFlow.pac}></FactorTag>
            <FactorTag label="Credit card debt" success={cashFlow.creditCardDebt}></FactorTag>
            <FactorTag label="Mortgage payments" success={cashFlow.mortgagePayments}></FactorTag>
            <FactorTag label="Savings" success={cashFlow.savings} open={openSavings} onClick={() => this.openTag('savings')}>
              {savingsPage === 1 ? this.renderSavingsContentPage1(cashFlow.plannedAnnualSavings, cashFlow.actualAnnualSavings) : this.renderSavingsContentPage2()}
            </FactorTag>
          </ContentPositioner>
        </BasePlate>
      </Page>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagingCashFlowScreen)
