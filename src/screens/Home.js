import React from 'react'
import styled from 'styled-components'
import Header from '../components/Header'
import RadialProgressBar from '../components/RadialProgressBar'
import { connect } from 'react-redux'
import { setActivePage } from '../state/actions'

const Page = styled.div`
  background-color: #504a8a;
  height: 100%;
`
const BasePlate = styled.div`
  background-image: url(gamma-baseplate.png);
  background-size: cover;
  position: absolute;
  width: 100%;
  top: 24%;
  height: 75%;
`
const HomeScreen = () => (
  <Page>
    <Header />
    <BasePlate>
      <GammaWheel />
    </BasePlate>
  </Page>
)

const GammaWheelContainer = styled.div`
  position: relative;
  margin-top: 75px;
`
const WheelContainer = styled.div`
  position: relative;
  width: 16em;
  height: 16em;
  padding: 2.8em;
  /*2.8em = 2em*1.4 (2em = half the width of a link with img, 1.4 = sqrt(2))*/
  margin: 0 auto;
`
const WheelNode = styled.a`
  display: block;
  position: absolute;
  top: 50%; left: 50%;
  width: 6em; height: 4em;
  margin: -2em -3em;
  transform: ${props => {
    /* eslint-disable */
    switch(props.pos) {
      case 1:
        return 'translateY(-8em)'
      case 2:
        return 'rotate(330deg) translate(8em) rotate(-330deg)'
      case 3:
        return 'rotate(30deg) translate(8em) rotate(-30deg)'
      case 4:
        return 'translateY(8em)'
      case 5:
        return 'rotate(150deg) translate(8em) rotate(-150deg)'
      case 6:
        return 'rotate(210deg) translate(8em) rotate(-210deg)'
    }
  }};
`
const GammaScoreContainer = styled.div`
  position: absolute;
  z-index: 2;
  width: 100px;
  height: 100px;
  top: 50%;
  left: 50%;
  margin-top: -50px;
  margin-left: -50px;
`
const GammaComponentPercent = styled.p`
  color: #fff;
  font-size: 20px;
  margin: 0;
  text-align: center;
`
const GammaComponentLabel = styled.p`
  color: #fff;
  font-size: 12px;
  text-align: center;
  margin: 0;
`
const GammaScoreLabel = styled.p`
  position: absolute;
  font-size: 8px;
  color: #172c4f;
  top: 56px;
  width: 52px;
  text-align: center;
  left: 23px;
  font-weight: bold;
`
function GammaComponent({ children, percent }) {
  return (
    <>
      <GammaComponentPercent>{percent}%</GammaComponentPercent>
      <GammaComponentLabel>{children}</GammaComponentLabel>
    </>
  )
}
const mapStateToProps = state => {
  return {
    cashFlow: state.cashFlow
  }
}
const mapDispatchToProps = dispatch => {
  return {
    gotoManagingCashFlow: () => dispatch(setActivePage('managing-cash-flow'))
  }
}
function GammaWheel({ gotoManagingCashFlow, cashFlow }) {
  let gammaScore = 72
  if (cashFlow.score >= 80) {
    gammaScore = 76
  }
  return (
    <GammaWheelContainer>
      <WheelContainer>
        <WheelNode pos={1}>
          <GammaComponent percent={85}>
            Optimizing your retirement
          </GammaComponent>
        </WheelNode>
        <WheelNode pos={2}>
          <GammaComponent percent={60}>
            Preparing for the unexpected
          </GammaComponent>
        </WheelNode>
        <WheelNode pos={3}>
          <GammaComponent percent={75}>
            Maximizing your business success
          </GammaComponent>
        </WheelNode>
        <WheelNode pos={4}>
          <GammaComponent percent={66}>
            Planning your major expenditures
          </GammaComponent>
        </WheelNode>
        <WheelNode pos={5} onClick={() => gotoManagingCashFlow()}>
          <GammaComponent percent={cashFlow.score}>
            Managing cash flow efficiently
          </GammaComponent>
        </WheelNode>
        <WheelNode pos={6}>
          <GammaComponent percent={90}>
            Sharing your wealth
          </GammaComponent>
        </WheelNode>
      </WheelContainer>
      <GammaScoreContainer>
        <RadialProgressBar percent={gammaScore} initialAnimation />
        <GammaScoreLabel>Your Gamma Score</GammaScoreLabel>
      </GammaScoreContainer>
    </GammaWheelContainer>
  )
}
/* eslint-disable */
GammaWheel = connect(mapStateToProps, mapDispatchToProps)(GammaWheel)

export default HomeScreen
