import React from 'react'

const Bot = () => {
  return (
    <iframe
      src='https://webchat.botframework.com/embed/teams1bot?s=c9pf6W0Grh0.cwA.nKQ.Nd013g2tYv0y57Gt4PkMGLeWo-gmmJt-_qhGJEwRZtQ'
      style={{
        //minWidth: '400px',
        width: '100%',
        height: '100%',
        minHeight: '500px',
      }}
    ></iframe>
  )
}

export default Bot
