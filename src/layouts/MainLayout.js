import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { setActivePage } from '../state/actions'
import NavBar from '../components/NavBar'

import Home from '../screens/Home'
import ManagingCashFlow from '../screens/ManagingCashFlow'
import Bot from '../screens/Bot'

const Content = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
  padding-bottom: 56px;
`
function mapStateToProps(state) {
  return state.layout
}
function mapDispatchToProps(dispatch) {
  return {
    setActivePage: page => dispatch(setActivePage(page))
  }
}
function getPageComponent(page) {
  if (page === 'home') {
    return <Home />
  } else if (page === 'managing-cash-flow') {
    return <ManagingCashFlow />
  } else if (page === 'favorites') {
    return <Bot />
  }
}
function MainLayout({ setActivePage, activePage }) {
  return (
    <>
      <HelmetWrapper />
      <Content>
        {getPageComponent(activePage)}
      </Content>
      <NavBar onNavChange={setActivePage} />
    </>
  )
}

function HelmetWrapper() {
  return (
    <Helmet
      title="Paradigmx"
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    >
      <html lang="en" />
      <body />
      <style type="text/css">{`
        html,
        body,
        #___gatsby,
        #___gatsby > div {
          height: 100%;
          margin: 0;
          padding: 0;
        }
        body {
          background-color: #fff;
          font-family: Montserrat, Verdana, sans-serif;
          min-height: 600px;
          overflow-y: hidden;
        }
        #___gatsby > div {
          position: relative;
        }
        div, p {
          box-sizing: border-box;
        }
        svg {
          overflow: visible;
        }
      `}</style>
    </Helmet>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout)
