import React from 'react'
import MainLayout from '../layouts/MainLayout'

const IndexPage = () => (
  <MainLayout />
)

export default IndexPage
